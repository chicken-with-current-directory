;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; A simple convenience procedure for changing directories.
;;;
;;; Also reexports `current-directory` from the process-contex module.
;;;
;;; Written by Evan Hanson <evhan@foldling.org> and placed in the Public
;;; Domain. All warranties are disclaimed.
;;;

(declare
  (module (with-current-directory))
  (import (chicken process-context))
  (export current-directory with-current-directory))

(define (with-current-directory path thunk)
  (let ((prev (current-directory)))
    (dynamic-wind
     (lambda () (change-directory path))
     thunk
     (lambda () (change-directory prev)))))
