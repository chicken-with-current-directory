with-current-directory
======================
Convenience procedure for temporarily changing directories.

Requirements
------------
 - [module-declarations](http://api.call-cc.org/doc/module-declarations)

API
---
A single procedure is provided, called `with-current-directory`. This
simply changes directory to the given `path`, calls `thunk`, and
restores the original working directory afterwards:

    (with-current-directory path thunk)

Note that **this procedure is not thread-safe**. The current directory
is a process-level value, and this extension makes no attempt to
preserve thread-specific working directories across interrupts. If the
calling thread is preempted or yields before the thunk's body returns,
the given path will persist as the process's current working directory
at least until the calling thread is resumed.

Author
------
Evan Hanson <evhan@foldling.org>

License
-------
Public Domain
